import pandas as pd
import xarray as xr
import pathlib
from ds_transformations import cordex_rotated_grid, slice_lon_lat
from compute import daily_mean
def read_data(fn):
    """Reads data from file.

    Args:
        fn: Filename/path of file containing the data.
        start_date: selects the start (datetime object YYYY-MM-DD) of the timeseries input data
        end_date: selects the end (datetime object YYYY-MM-DD) of the timeseries input data.

    Returns: xarray dataset containing the data between start_date and end_date
    """
    print("Check filetype")

    # Check the extension of the data file:
    file_type = pathlib.Path(fn).suffix

    if file_type == ".nc" or file_type == ".nc_ET":
        print("Read netCDF file")
        with xr.open_dataset(fn) as ds:
            ds.load()
        # ds = ds.rename_vars({"temperature_2_meter": "t"})
        # ds = ds.sel(latitude=slice(*latitude_range)).mean(["longitude", "latitude"])

        """
        df = ds.to_dataframe()
        print(df.head())
        # Dropping the columns having NaN/NaT values
        df = df.dropna()

        # Resetting the indices using df.reset_index()
        df = df.reset_index(drop=True)

        ds = df.to_xarray()
        """
        return ds

    """
    if file_type == ".nc_ET":
        print("Read netCDF file")
        with xr.open_dataset(fn) as ds:
            ds.load()
    """

    if file_type == ".csv":
        print("Read csv file")
        df = pd.read_csv(fn, parse_dates=["time"], index_col="time")

        ds = pd.DataFrame.to_xarray(df)


        """
        # SI Units °C to K
        ds["t"].data = ds["t"].data + 273.15
        ds["t"].attrs["units"] = "K"
        """
    return ds

def get_all_datasets(orographie_file, fn, fn_cclm):
    """takes filenames of orographie file, SPARTACUs data and cclm simulation
    and reads them as xarray datasets and gets them in the structure: dimensions: time, rlat, rlon, data_vars: Tn, orog
    Tn: daily minimum temperature, orog: height of the gridpoint
    input:
    orographie_file
    fn: filename to SPARTACUS Dataset containing Tn
    fn_cclm: filename to CCLM simulation in rotated pole grid: containing T_2M

    output:
    ds (xarray ds): dims: time, rlat, rlon; data_vars: orog, Tn
    ds_cclm (xarray ds): dims: time, rlat, rlon; data_vars: orog, T_2M
    cclm_daily_min (xarray ds): dims: time, rlat, rlon; data_vbars: orog, Tn"""

    ds_cclm = read_data(fn_cclm)

    ds_orographie = read_data(orographie_file)
    ds_orographie = cordex_rotated_grid(ds_orographie, ds_cclm)


    ds = read_data(fn)
    ds = cordex_rotated_grid(ds, ds_cclm)
    ds = ds.assign(orog=ds_orographie['dem'])  # data_vars: Tn, Tx and orog: height of lon lat
    ds = daily_mean(ds)

    ds_cclm = ds_cclm[['T_2M']]  # drop all other data_vars
    ds_cclm = ds_cclm.assign(orog=ds_orographie['dem'])
    #ds_cclm = ds_cclm.drop_dims('height_2m').drop_dims('height_10m')
    ds_cclm = slice_lon_lat(ds_cclm, min_lat=46.300395, max_lat=49.191016, min_lon=9.372346,
                                   max_lon=17.390089)

    cclm_daily_min = ds_cclm.T_2M.groupby('time.day').min().to_dataset()
    cclm_daily_min = cclm_daily_min.rename(T_2M='Tn')
    cclm_daily_min = cclm_daily_min.assign(orog=ds_orographie['dem'])  # ds: dims: lon, lat, time=day;
    # data_vars:Tn, orog
    cclm_daily_min = slice_lon_lat(cclm_daily_min, min_lat=46.300395, max_lat=49.191016, min_lon=9.372346,
                                   max_lon=17.390089)

    return ds, ds_cclm, cclm_daily_min
def write_data(data, output_dir=""):
    print("Write data to netCDF file")
    data_nc = data.to_netcdf(output_dir)

    return data_nc
