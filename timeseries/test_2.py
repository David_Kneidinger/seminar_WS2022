import scipy.optimize
import numpy as np
"""
class FuncyFunc:
    def __init__(self, redcurveascend):  # Davids geiler Parameter von außen
        self.redcurveascend = redcurveascend

    def target(self, y, p1):
        return self.redcurveascend * y + p1

X = np.linspace(-1, 1, 100)
Y = 0.8 * X + np.random.normal(scale=0.03, size=X.shape)

ff = FuncyFunc(0.3)
p, e = scipy.optimize.curve_fit(ff.target, X, Y)
"""
X = np.linspace(-1, 1, 100)
Y = 0.8 * X + np.random.normal(scale=0.03, size=X.shape)

def target(y, p2, redcurveascend):
    return (redcurveascend) * y + p2
p, e = scipy.optimize.curve_fit(lambda y, p2: target(y, p2, redcurveascend=0.3), X, Y)

print(p)