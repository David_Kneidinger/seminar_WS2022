import numpy as np
import matplotlib.pyplot as plt
from input_output import read_data
from compute import find_initial_guess, piecewise_linear
import matplotlib.dates as mdates
from scipy import optimize
from operator import itemgetter
import itertools
def roundup(x):
    return x if x % 100 == 0 else x + 100 - x % 100


import numpy as np

# Create the test data
temp = [-0.7923077023946322, -1.0357143126782917, -1.5200000405311584, -2.1136364286596123, -2.5111111534966364, -2.8105263710021973, -2.083333353201548, -1.90000006225374, -1.6142857415335519, -0.4000000059604645, 0.25]
height = [350.,  450., 550., 650., 750.,  850., 950., 1050., 1150., 1250., 1350.]

temp = [0, -1, -2, -3, -4, -5, -4, -3, -2, -1]
height = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
plt.figure()
plt.scatter(temp, height, marker='.', s=50, alpha=1, color='orange')

p0 = find_initial_guess(np.array(temp), np.array(height))
print('temp', temp)
print('height', height)
print(p0)
#p0 = [0, -5, 1/10, 1/10]
p, e = optimize.curve_fit(piecewise_linear, height, temp, p0)
print(p)
xd = np.linspace(min(height), max(height), 100)
plt.plot(piecewise_linear(xd, *p), xd, color='orange')

plt.show()