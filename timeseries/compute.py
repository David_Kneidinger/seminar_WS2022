import datetime
import xarray as xr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
from ds_transformations import slice_lon_lat
from operator import itemgetter
import itertools


def yearly_mean(ds):
    ds = ds.groupby("time.year").mean()
    return ds


def monthly_mean(ds):
    ds = ds.groupby("time.month").mean()
    return ds


def weighted_temporal_mean(ds, var):
    """
    weight by days in each month
    """
    # Determine the month length
    month_length = ds.time.dt.days_in_month

    # Calculate the weights
    wgts = month_length.groupby("time.year") / month_length.groupby("time.year").sum()

    # Make sure the weights in each year add up to 1
    np.testing.assert_allclose(wgts.groupby("time.year").sum(xr.ALL_DIMS), 1.0)

    # Subset our dataset for our variable
    obs = ds[var]

    # Setup our masking for nan values
    cond = obs.isnull()
    ones = xr.where(cond, 0.0, 1.0)

    # Calculate the numerator
    obs_sum = (obs * wgts).resample(time="AS").sum(dim="time")

    # Calculate the denominator
    ones_out = (ones * wgts).resample(time="AS").sum(dim="time")

    # Return the weighted average
    return obs_sum / ones_out


def daily_mean(ds):
    ds = ds.groupby("time.day").mean("time")
    return ds


def mean_DJF(ds):
    ds_DJF = ds.where(ds['time.season'] == 'DJF')
    ds_DJF = ds_DJF.rolling(min_periods=3, center=True, time=3).mean()

    # make annual mean
    ds_DJF = ds_DJF.groupby('time.year').mean('time').mean(dim="year")

    return ds_DJF


def mean_JJA(ds):
    ds_JJA = ds.where(ds['time.season'] == 'JJA')
    ds_JJA = ds_JJA.rolling(min_periods=3, center=True, time=3).mean()

    # make annual mean
    ds_JJA = ds_JJA.groupby('time.year').mean('time').mean(dim="year")

    return ds_JJA


def find_maximum_ozone(ds):
    """finds the height of the maximum level of ozone
    input:
    ds (xarray): dataset with ozone as data variable
    output:
    ds (xarray): dataset with height of maximum ozone layer"""

    # index = ds.ozone.argmax(dim="altitude")
    ds = ds.ozone
    ds = ds.idxmax('altitude')
    return ds


def get_daily_sunshine(ds):
    """calculates the sunshine time per day for a given dataset
    input:
    ds(xarray): xarray dataset containing sunhine SUX
    output:
    ds (xarray): containing the amount of sunshine per day"""

    ds = ds.groupby("time.day").sum("time")
    return ds


def get_label_unit(ds):
    if ds.attrs["units"] == "degree_Celsius":
        unit = "$\\circ C$"
    if ds.attrs["units"] == "K":
        unit = "$K$"
    if ds.attrs["units"] == "m":
        unit = "$m$"
    return unit


def anomalies(ds, start_date, end_date):
    """Compute monthly anomalies for the given data, relative to some reference time.

    Args:

        ds (xarray dataset): Data for which the monthly anomalies
            are computed. The dataset must contain a valid datetime
            index.
        start_date (datetime.date): Selected start date in ISO-format
        end_date (datetime.date): Selected end date in ISO-format


    Returns: pandas dataframe containing timeseries of monthly anomalies.

    """

    print("Compute anomalies.")

    clim_ref_period = (
        datetime.date.fromisoformat("1991-01-01"),
        datetime.date.fromisoformat("2020-12-31"),
    )

    clim = ds.sel(time=slice(*clim_ref_period)).groupby("time.month").mean()

    if start_date and end_date:
        ds = ds.sel(time=slice(start_date, end_date))

    if start_date and not end_date:
        print("start")
    if end_date and not start_date:
        print("end")

    anom = ds.groupby("time.month") - clim

    return anom


def linear_trend(ds, start_date, end_date):
    """Computes a linear trend for given data.

    Args:
        ds(xarray dataset):  The dataset must contain a valid datetime
            index.
        start_date (datetime.date): Input start date of the plotted ds

        end_date (datetime.date): Input end date of the plotted ds

    Returns: an xarray dataset containing the data and values ("trend" )to plot a linear trend
    """

    if start_date and end_date:
        ds = ds.sel(time=slice(start_date, end_date))

    if start_date and not end_date:
        print("I was not able to fix this yet")
    if end_date and not start_date:
        print("I was not able to fix this yet")

    time = np.arange(ds["t"].dropna("time").shape[0])
    temp = ds["t"].dropna("time").values

    coef = np.polyfit(time, temp, deg=1)
    time_fit = np.arange(ds["t"].shape[0])
    temp_fit_func = np.poly1d(coef)
    temp_fit = temp_fit_func(time_fit)

    ds = ds.assign({"trend": ds["t"] * 0 + temp_fit})

    return ds


def drop_nan_for_all_data_vars(ds_temp, ds_orog):
    """drops all nan values for all data_vars
    input:
    ds_temp (xarray data_vars): xarray data_vars e.g. ds.Tn
    ds_orog (xarray data_vars): ds.orog
    output:
    temp (np.array): without nan
    orog (np.array): without nan"""

    temp = np.array(ds_temp.values).ravel()
    temp_nan = np.array(ds_temp.values).ravel()

    height = np.array(ds_orog.values).ravel()
    height_nan = np.array(ds_orog.values).ravel()

    temp_nan[np.invert(np.isnan(temp_nan))] = 1
    height_nan[np.invert(np.isnan(height_nan))] = 1

    temp = temp * height_nan
    temp = temp[np.isfinite(temp)]

    height = height * temp_nan
    height = height[np.isfinite(height)]

    return temp, height


def calc_uls_lapse_rate(temp, height):
    """calulates lapse rate from upper level stations
    input:#
    temp (np.array): containing upper level data points (above 2000m)
    height (np.array)
    output:
    gamma (float): lapse rate / slope of the fit"""

    p = np.polyfit(temp, height, 1)

    return p[0], p[1]


def piecewise(y, a, h0, h1, gamma, T0):
    """
    return np.piecewise(z, [z <= h0, ((z > h0) & (z < h1)), z >= h1], [lambda z: T0 - gamma*z - a,
                                                                       lambda z: T0 - gamma*z - a/2*(1+np.cos(np.pi*(z-h0)/(h1-h0))),
                                                                       lambda z: T0 - gamma*z])
    """
    condlist = [y <= h0, (y > h0) & (y < h1), y >= h1]
    funclist = [lambda y: T0 - gamma * y - a,
                lambda y: T0 - gamma * y - a / 2 * (1 + np.cos(np.pi * (y - h0) / (h1 - h0))),
                lambda y: T0 - gamma * y]
    return np.piecewise(y, condlist, funclist)


def piecewise_linear(x, x0, y0, k1, k2):
    return np.piecewise(x, [x < x0], [lambda x: -k1 * x + y0 + k1 * x0, lambda x: k2 * x + y0 - k2 * x0])


def linear_function(x, k, d):
    return k * x + d


def background_temperature_field(ds):  # TODO: do it for all subregions
    """takes a dataset of Austria with temperature and orog in it and return a background temperature field
    input:
    ds (xarray): xarray dataset containg temperature Tn or Tx and orog data from domain
    output:
    ds (xarray): xarray containing: magnitude, thickness and intensity (only for lon lat of domain)
    """

    # get upper level grid points uls for lapse rate estimation (grid points above 2000 m)
    ds_uls = ds.where(ds.orog > 2200, drop=True)  # does only reduce orog not the whole data set :(

    # make sure ground effects such as cold pools are neglected TODO: does not work this way
    """
    res = ds.Tx.values - ds.Tn.values
    res = res[np.isfinite(res)]
    ds = ds.where(ds.Tx - ds.Tn < np.mean(res) + np.var(res))
    """
    ds = ds.where(ds.orog > 500, drop=True)

    fig, axs = plt.subplots(nrows=3, ncols=2, sharey=True, sharex=True)  # TODO: make subplots automattically
    axs = axs.flatten()

    index = 0
    # ds.time.dt.day.values
    # [12, 28]
    for i, ax in zip(ds.day.values, axs):
        ds_uls_ = ds_uls.sel(day=i)
        # time=ds.time.dt.day == i
        ds_ = ds.sel(day=i)

        temp_uls, height_uls = drop_nan_for_all_data_vars(ds_uls_.Tn, ds_uls_.orog)
        temp_uls, height_uls, _ = altitudinal_zones(temp_uls, height_uls)

        gamma, T0 = calc_uls_lapse_rate(height_uls, temp_uls)

        lin_ = np.linspace(np.min(height_uls), np.max(height_uls), 1000)
        ax.plot(gamma * lin_ + T0, lin_, 'r')

        temp, height = drop_nan_for_all_data_vars(ds_.Tn, ds_.orog)
        ax.scatter(temp, height, marker='.', s=5, alpha=0.1)
        lin_height = np.linspace(np.min(height), np.max(height), 1000)

        """
        p0 = [1, 1000, 1500]  # initial guess for [a, h0, h1]

        p, e = optimize.curve_fit(lambda y, a, h0, h1: piecewise(y, a, h0, h1, gamma=gamma, T0=T0), height, temp, p0)

        lin_height = np.linspace(np.min(height), np.max(height), 1000) #TODO: set gamma and T0 fixed in fit does not lead to good results
        ax.plot(piecewise_linear(lin_height, *p, gamma, T0), lin_height, 'orange')
        """
        if i != 22:  # explicitly exclude 22.12.2009 from the fit
            p0 = [1, 1000, 1500, gamma, T0]  # initial guess

            p, e = optimize.curve_fit(lambda y, a, h0, h1, gamma, T0: piecewise(y, a, h0, h1, gamma, T0), height, temp,
                                      p0)

            h0 = p[1]
            h1 = p[2]
            a = p[0]
            ds = ds.assign(magnitude=ds.Tn / ds.Tn * a)
            ds['magnitude'].attrs["Units"] = "°C"
            ds = ds.assign(thickness=ds.Tn / ds.Tn * (h1 - h0))
            ds['thickness'].attrs["Units"] = "m"
            ds = ds.assign(intensity=ds.Tn / ds.Tn * (h1 - h0) / a)
            ds['intensity'].attrs["Units"] = "°C / m"

            ax.plot(piecewise(lin_height, *p), lin_height, 'g--')

            # --------------------------------------------------------------------------------------------------------------
        # plot
        temp, height, _ = altitudinal_zones(temp, height)
        ax.scatter(temp, height, marker='.', s=5, alpha=0.1)
        # ax.set_title(f'{i}.{ds.time.dt.month.values[index]}.{ds.time.dt.year.values[index]}')  # TODO: can be done better
        ax.set_title(f'{i}.12.2009', fontsize=10)
        ax.set_xlabel("T (°C)")
        ax.set_ylabel(f"h (m)")
        ax.label_outer()

        index += 1

    return ds


def low_level_temperature_field(ds):
    fig, axs = plt.subplots(nrows=3, ncols=2, sharey=True, sharex=True)  # TODO: make subplots automattically
    axs = axs.flatten()

    for i, ax in zip(ds.day.values, axs):
        ds_ = ds.sel(day=i)
        temp, height = drop_nan_for_all_data_vars(ds_.Tn, ds_.orog)
        ax.scatter(temp, height, marker='.', s=50, alpha=0.1, color='blue')
        temp, height = altitudinal_zones(temp, height)
        ax.scatter(temp, height, marker='.', s=50, alpha=1, color='orange')

        p0 = find_initial_guess(np.array(temp), np.array(height))
        p, e = optimize.curve_fit(piecewise_linear, height, temp, p0)
        xd = np.linspace(min(height), max(height), 100)
        ax.plot(piecewise_linear(xd, *p), xd, color='orange')
        ax.set_xlabel("T (°C)")
        ax.set_ylabel(f"h (m)")
        ax.set_title(f'{i}.12.2009', fontsize=10)
        ax.label_outer()

    return None


def full_temperature_profile_piecewise(ds, ds_subregion):
    fig, axs = plt.subplots(nrows=3, ncols=2, sharey=True, sharex=True)  # TODO: make subplots automattically
    axs = axs.flatten()

    # get upper level grid points uls for lapse rate estimation (grid points above 2000 m)
    ds_uls = ds_subregion.where(ds_subregion.orog > 2200, drop=True)  # does only reduce orog not the whole data set :(
    ds_subregion = ds_subregion.where(ds_subregion.orog > 500, drop=True)

    for i, ax in zip(ds.day.values, axs):
        ds_uls_ = ds_uls.sel(day=i)
        ds_subregion_ = ds_subregion.sel(day=i)
        ds_ = ds.sel(day=i)

        temp_uls, height_uls = drop_nan_for_all_data_vars(ds_uls_.Tn, ds_uls_.orog)
        temp_uls, height_uls, _ = altitudinal_zones(temp_uls, height_uls)

        gamma, T0 = calc_uls_lapse_rate(height_uls, temp_uls)

        lin_ = np.linspace(np.min(height_uls), np.max(height_uls), 1000)
        # ax.plot(gamma * lin_ + T0, lin_, 'r')


        temp, height = drop_nan_for_all_data_vars(ds_subregion_.Tn, ds_subregion_.orog)
        #ax.scatter(temp, height, marker='.', s=5, alpha=0.1, color='green')
        lin_height = np.linspace(np.min(height), np.max(height), 1000)

        if i != 22:  # explicitly exclude 22.12.2009 from the fit
            p0 = [1, 1000, 1500, gamma, T0]  # initial guess

            p_btf, cov_btf = optimize.curve_fit(lambda y, a, h0, h1, gamma, T0: piecewise(y, a, h0, h1, gamma, T0),
                                                height, temp,
                                                p0)


        temp, height = drop_nan_for_all_data_vars(ds_.Tn, ds_.orog)
        #ax.scatter(temp, height, marker='.', s=50, alpha=0.1, color='blue')
        temp, height, std_temp = altitudinal_zones(temp, height)

        ax.scatter(temp, height, marker='.', s=50, alpha=1, color='orange')
        p0 = find_initial_guess(np.array(temp), np.array(height))
        #, method='trf'
        p, cov = optimize.curve_fit(piecewise_linear, height, temp, sigma=std_temp, p0=p0)

        xd = np.linspace(min(height), max(height), 100)
        ax.plot(piecewise_linear(xd, *p), xd, color='orange')
        
        # plot confidence intervall for low level temp------------------------------------------------------------------
        sig = np.sqrt(np.diagonal(cov))
        bound_upper = piecewise_linear(xd, *(p + sig))
        bound_lower = piecewise_linear(xd, *(p - sig))
        ax.fill_betweenx(xd, bound_lower, bound_upper, color='orange', alpha=0.4)

        lin_height = np.linspace(np.max(ds_.orog), np.max(ds_subregion_.orog), 100)
        y, x, diff_y, diff_x = match_functions(piecewise_linear(xd, *p), xd, piecewise(lin_height, *p_btf), lin_height)

        # plot condfidence intervall for btf ---------------------------------------------------------------------------
        sig_btf = np.sqrt(np.diagonal(cov_btf))
        bound_upper_btf = piecewise(lin_height + diff_x, *(p_btf + 2*sig_btf))
        bound_lower_btf = piecewise(lin_height - diff_x, *(p_btf - 2*sig_btf))
        # p_btf = [+a, -h0, +h1, +gamma, +T0]
        sig_btf = sig_btf
        p_btf_upper = np.array([p_btf[0]+sig_btf[0], p_btf[1],
                       p_btf[2], p_btf[3], p_btf[4]+sig_btf[4] + diff_y])
        p_btf_lower = np.array([p_btf[0]-sig_btf[0], p_btf[1],
                       p_btf[2], p_btf[3], p_btf[4]-sig_btf[4] - diff_y])
        bound_upper_btf = piecewise(lin_height, *(p_btf_upper))
        bound_lower_btf = piecewise(lin_height, *(p_btf_lower))
        ax.plot(y, x, 'g-')
        ax.fill_betweenx(x, bound_lower_btf - diff_y, bound_upper_btf - diff_y, color='orange', alpha=0.4)

        ax.set_xlabel("T (°C)")
        ax.set_ylabel(f"h (m)")
        ax.set_title(f'{i}.12.2009', fontsize=10)
        ax.label_outer()

    return None


def full_temperature_profile(ds, ds_subregion):
    fig, axs = plt.subplots(nrows=3, ncols=2, sharey=True, sharex=True)  # TODO: make subplots automattically
    axs = axs.flatten()

    # get upper level grid points uls for lapse rate estimation (grid points above 2000 m)
    ds_uls = ds_subregion.where(ds_subregion.orog > 2200, drop=True)  # does only reduce orog not the whole data set :(
    ds_subregion = ds_subregion.where(ds_subregion.orog > 500, drop=True)

    for i, ax in zip(ds.day.values, axs):
        ds_uls_ = ds_uls.sel(day=i)
        ds_subregion_ = ds_subregion.sel(day=i)
        ds_ = ds.sel(day=i)

        temp_uls, height_uls = drop_nan_for_all_data_vars(ds_uls_.Tn, ds_uls_.orog)
        temp_uls, height_uls, _ = altitudinal_zones(temp_uls, height_uls)

        gamma, T0 = calc_uls_lapse_rate(height_uls, temp_uls)

        lin_ = np.linspace(np.min(height_uls), np.max(height_uls), 1000)
        # ax.plot(gamma * lin_ + T0, lin_, 'r')

        temp, height = drop_nan_for_all_data_vars(ds_subregion_.Tn, ds_subregion_.orog)
        # ax.scatter(temp, height, marker='.', s=5, alpha=0.1, color='green')
        lin_height = np.linspace(np.min(height), np.max(height), 1000)

        # fit of background temperature field---------------------------------------------------------------------------
        p0 = [1, 1000, 1500, gamma, T0]  # initial guess

        p_btf, cov_btf = optimize.curve_fit(lambda y, a, h0, h1, gamma, T0: piecewise(y, a, h0, h1, gamma, T0),
                                            height, temp,
                                            p0)
        # fit of low level temperature----------------------------------------------------------------------------------
        temp, height = drop_nan_for_all_data_vars(ds_.Tn, ds_.orog)
        ax.scatter(temp, height, marker='.', s=50, alpha=0.4, color='blue')
        temp, height, std_temp = altitudinal_zones(temp, height)

        p0 = find_initial_guess(np.array(temp), np.array(height))
        p, cov = optimize.curve_fit(piecewise_linear, height, temp, sigma=std_temp, p0=p0)

        xd = np.linspace(min(height), max(height), 100)

        lin_height = np.linspace(np.max(ds_.orog), np.max(ds_subregion_.orog), 100)
        y, x, diff_y, diff_x = match_functions(piecewise_linear(xd, *p), xd, piecewise(lin_height, *p_btf), lin_height)

        # plot condfidence intervall for btf ---------------------------------------------------------------------------
        sig_btf = np.sqrt(np.diagonal(cov_btf))
        # p_btf = [+a, -h0, +h1, +gamma, +T0]
        sig_btf = sig_btf
        p_btf_upper = np.array([p_btf[0] + sig_btf[0], p_btf[1],
                                p_btf[2], p_btf[3], p_btf[4] + sig_btf[4] + diff_y])
        # calculate full temp profile-----------------------------------------------------------------------------------
        height_upper = lin_height
        temp_upper = list(y)
        std_temp_upper = list(piecewise(height_upper, *p_btf_upper) - piecewise(height_upper, *p_btf))

        # get full temp, height, std
        temp = list(temp) + temp_upper
        height = list(height) + list(height_upper)
        std = list(std_temp) + std_temp_upper

        #ax.scatter(temp, height, marker='.', s=50, alpha=0.5)

        # fit full temp profil with piecewise function------------------------------------------------------------------
        p_btf, cov_btf = optimize.curve_fit(lambda y, a, h0, h1, gamma, T0: piecewise(y, a, h0, h1, gamma, T0),
                                            height, temp,
                                            p0=p_btf, sigma=std, absolute_sigma=True)
        lin_height = np.linspace(np.min(ds_.orog), np.max(ds_subregion_.orog), 100)

        # plot confidence intervall of full temperature profile---------------------------------------------------------
        p_btf_upper = np.array([p_btf[0] + sig_btf[0], p_btf[1],
                                p_btf[2], p_btf[3], p_btf[4] + sig_btf[4] + diff_y])
        p_btf_lower = np.array([p_btf[0] - sig_btf[0], p_btf[1],
                                p_btf[2], p_btf[3], p_btf[4] - sig_btf[4] - diff_y])
        bound_upper_btf = piecewise(lin_height, *(p_btf_upper))
        bound_lower_btf = piecewise(lin_height, *(p_btf_lower))
        ax.fill_betweenx(lin_height, bound_lower_btf, bound_upper_btf, color='orange', alpha=0.4)

        #plot-----------------------------------------------------------------------------------------------------------
        ax.plot(piecewise(lin_height, *p_btf), lin_height)
        #ax.plot(y, x, 'g-')
        #ax.fill_betweenx(x, bound_lower_btf - diff_y, bound_upper_btf - diff_y, color='orange', alpha=0.4)

        ax.set_xlabel("T (°C)")
        ax.set_ylabel(f"h (m)")
        ax.set_title(f'{i}.12.2009', fontsize=10)
        ax.label_outer()
    return None

def roundup(x):
    return x if x % 100 == 0 else x + 100 - x % 100


def altitudinal_zones(temp, height):
    """calculates the mean of every 100 m altitudinal zone
        intput:
        temp (np.array)
        height (np.array)
        :return
        zone_temps (np.array): containg the mean of each 100 m altitudinal zone"""
    zone = 100

    alt_zones = np.arange(0, roundup(max(height)), zone)
    temp_zones = []
    alt_zone = []
    std_temp = []
    for i in range(len(alt_zones) - 1):
        alt_zone.append((alt_zones[i] + alt_zones[i + 1]) / 2)
        index = list(np.where((height > alt_zones[i]) & (height < alt_zones[i + 1])))
        if len(index[0]) > 0:
            res_list = list(itemgetter(*index)(temp))
            sig = np.std(res_list)
            if sig > 0:
                temp_zones.append(np.mean(res_list))
                std_temp.append(sig)
            else:
               temp_zones.append(np.nan)
        else:
            temp_zones.append(np.nan)

    nan_index = np.isfinite(temp_zones)
    temp_zones = list(itertools.compress(temp_zones, nan_index))
    alt_zone = alt_zone * nan_index
    alt_zone = alt_zone[alt_zone != 0]
    #std_temp = np.array(std_temp)
    #std_temp = list(np.where(std_temp == 0, np.inf, std_temp))

    return temp_zones, alt_zone, std_temp


def find_initial_guess(temp, height):
    """get temp and height for low_level_temperature_field
    and calculates the inital guesses for the curve fit of the piecewise linear function
    input:
    temp (np.array)
    height (np.array)
    output:
    p0 (list): containing the intial guess for the fit"""

    local_min_index = np.where(temp == temp.min())[0][0]  # TODO: dont know why i need [0][0]
    local_max_index = np.where(temp == temp.max())[0][0]
    if height[local_min_index] > min(height):  # assures that the minimum is a local minimum
        # due to an inversion above the ground
        local_min = True
        p1 = np.polyfit(temp[:local_min_index], height[:local_min_index], deg=1)
        T0 = height[local_min_index]
        p2 = np.polyfit(temp[local_min_index:], height[local_min_index:], deg=1)
        p0 = [T0, temp.min(), 1 / p1[0], 1 / p2[0]]
    else:
        p0 = [1, 1, 1, 1]
    return p0


def match_functions(y1, x1, y2, x2):
    diff_y = y2[0] - y1[-1]
    diff_x = x2[0] - x1[-1]

    # shift function 2 by diff_y and diff_x
    y2 = y2 - diff_y
    x2 = x2 - diff_x
    return y2, x2, diff_y, diff_x
