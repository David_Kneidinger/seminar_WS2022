import cartopy.crs as ccrs
import numpy as np
from math import *
import cordex as cx
import xesmf as xe

def transform_projection_coordinates(ds):

    # Example - your x and y coordinates are in a Lambert Conformal projection
    data_crs = ccrs.LambertConformal(central_longitude=-100)

    # Transform the point - src_crs is always Plate Carree for lat/lon grid
    x, y = data_crs.transform_point(-122.68, 21.2, src_crs=ccrs.PlateCarree())

    # Now you can select data
    ds.sel(x=x, y=y)
    return ds

def slice_lon_lat(ds, min_lat, max_lat, min_lon, max_lon):
    """reduces spatial size of ds
    input:
    ds (xarray): xarray dataset containing: dims:lon, lat
    min_lat, max_lat: definig the new latitude range
    min_lon, max_lon: defining the new longitude range

    output:
    ds (xarray): xarray dataset in the spatial region between min_lat, max_lat and min_lon, max_lon"""

    ds = ds.where((ds.coords['lat'] > min_lat) & (ds.coords['lat'] < max_lat) & (ds.coords['lon'] > min_lon)
                  & (ds.coords['lon'] < max_lon))

    return ds

def austrian_subregions(ds):
    """devides austria in three subregions
    input:
    #ds (xarray): xarray dataset of austria
    output:
    """

    br = np.array([11.451769, 46.945871])  # [lon, lat] Brenner
    wn = np.array([16.23318, 47.80279])  # [lon, lat] Wiener Neustadt
    k = (wn[1] - br[1]) / (wn[0] - br[0])  # slope lon/lat

    # South domain
    south = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) < 0) & (ds.coords['lon'] > br[0])
                     , drop=True)

    # North west domain
    nw = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) > 0) & (ds.coords['lon'] <= 12.95)
                  , drop=True)

    # North east domain
    ne = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) > 0) & (ds.coords['lon'] >= 12.95)
                  , drop=True)

    return south, nw, ne

def austrian_subregions_cclm(ds_orog, ds_T, subregion):
    """devides austria in three subregions
    input:
    ds_orog (xarray): xarray dataset of austria containing orog
    ds_T (xarray): xarray dataset of austria containing Tn or Tx
    subregion (string): disred subregion: str(south), nort-west: str(nw), north-east: str(ne)
    output:
    """

    br = np.array([11.451769, 46.945871])  # [lon, lat] Brenner
    wn = np.array([16.23318, 47.80279])  # [lon, lat] Wiener Neustadt
    k = (wn[1] - br[1]) / (wn[0] - br[0])  # slope lon/lat


    for ds in [ds_T, ds_orog]:
        if subregion == 'south':

            ds = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) < 0) & (ds.coords['lon'] > br[0])
                          , drop=True)

        if subregion == 'nw':
            ds = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) > 0) & (ds.coords['lon'] <= 12.95)
                          , drop=True)

        if subregion == 'ne':
            ds = ds.where((ds.coords['lat'] - ((ds.coords['lon'] - br[0]) * k + br[1]) > 0) & (ds.coords['lon'] >= 12.95)
                          , drop=True)

    return ds_T, ds_orog

def get_cclm_to_spartacus_area(cclm, ds):  # TODO: does not work
    r_lon =0.12968063
    r_lat = 0.00434875

    cclm = cclm.where((cclm.coords['lon'] <= ds.coords['lon'] + r_lon or cclm.coords['lon'] >= ds.coords['lon'] + r_lon)
                   & (cclm.coords['lat'] <= ds.coords['lat'] + r_lat or cclm.coords['lat'] >= ds.coords['lat'] + r_lat))
    return cclm


def rotated_grid_transform(grid_in, option, SP_coor):  #TODO: not finished
    lon = grid_in[0]
    lat = grid_in[1];

    lon = (lon * pi) / 180;  # Convert degrees to radians
    lat = (lat * pi) / 180;

    SP_lon = SP_coor[0];
    SP_lat = SP_coor[1];

    theta = 90 + SP_lat;  # Rotation around y-axis
    phi = SP_lon;  # Rotation around z-axis

    theta = (theta * pi) / 180;
    phi = (phi * pi) / 180;  # Convert degrees to radians

    x = cos(lon) * cos(lat);  # Convert from spherical to cartesian coordinates
    y = sin(lon) * cos(lat);
    z = sin(lat);

    if option == 1:  # Regular -> Rotated

        x_new = cos(theta) * cos(phi) * x + cos(theta) * sin(phi) * y + sin(theta) * z;
        y_new = -sin(phi) * x + cos(phi) * y;
        z_new = -sin(theta) * cos(phi) * x - sin(theta) * sin(phi) * y + cos(theta) * z;

    else:  # Rotated -> Regular

        phi = -phi;
        theta = -theta;

        x_new = cos(theta) * cos(phi) * x + sin(phi) * y + sin(theta) * cos(phi) * z;
        y_new = -cos(theta) * sin(phi) * x + cos(phi) * y - sin(theta) * sin(phi) * z;
        z_new = -sin(theta) * x + cos(theta) * z;

    lon_new = atan2(y_new, x_new);  # Convert cartesian back to spherical coordinates
    lat_new = asin(z_new);

    lon_new = (lon_new * 180) / pi;  # Convert radians back to degrees
    lat_new = (lat_new * 180) / pi;

    print
    lon_new, lat_new;

rotated_grid_transform((0, 0), 1, (0, 30))


def cordex_rotated_grid(ds, ds_cordex):
    regridder = xe.Regridder(ds, ds_cordex, "bilinear", periodic=True)
    ds = regridder(ds)

    return ds
