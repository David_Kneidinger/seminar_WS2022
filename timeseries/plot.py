import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import matplotlib.dates as mdates
import cartopy
import cartopy.crs as ccrs
import numpy as np
from compute import get_label_unit, get_daily_sunshine, drop_nan_for_all_data_vars, calc_uls_lapse_rate, piecewise_linear
from scipy import optimize

def plot_2dmap(ds, map):
    """plots 2 map of ds
    input:
    ds (xarray)
    map (string): orog for orography plot and T for temperature plot
    output:
    plot"""

    lons = ds.coords["lon"]
    lats = ds.coords["lat"]
    values = ds.values

    #ax = plt.axes(projection=ccrs.PlateCarree())
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})


    # the keyword "transform" tells cartopy in which projection the data is stored

    if map == 'orog':
        map = 'terrain'
        unit = 'm'
    if map == 'T':
        unit = 'K'
        map = 'coolwarm'

    cb = plt.contourf(lons, lats, values, cmap=map, transform=ccrs.PlateCarree())
    plt.colorbar(cb)

    ax.coastlines()
    gl = ax.gridlines(draw_labels=True)  # Add gridlines and coastlines to the plot
    gl.top_labels = False
    gl.right_labels = False

    #unit = get_label_unit(ds)

    clb = fig.colorbar(cb, ax=ax, shrink=0.3)
    clb.set_label(unit, labelpad=-35, y=1.07, rotation=0)

def plot_comparisson(ds1, ds2):
    ds = [ds1, ds2]

    fig, axs = plt.subplots(nrows=1, ncols=2, sharey=True, subplot_kw={'projection': ccrs.PlateCarree()})
    axs = axs.flatten()

    title = ["daily minimum temperature 2009-12-17", "daily minimum temperature 2009-12-24"]
    for i, ax in zip(range(len(ds)), axs):

        lons = ds[i].coords["lon"]
        lats = ds[i].coords["lat"]
        values = ds[i].values

        # the keyword "transform" tells cartopy in which projection the data is stored
        #check what lambert conformal mapping has to do with that
        cb = axs[i].contourf(lons, lats, values, cmap='coolwarm', transform=ccrs.PlateCarree())

        axs[i].coastlines()
        gl = axs[i].gridlines(draw_labels=True)  # Add gridlines and coastlines to the plot
        gl.top_labels = False
        gl.right_labels = False
        axs[i].set_title(title[i])

    unit = get_label_unit(ds1)

    clb = fig.colorbar(cb, ax=axs, shrink=0.3)
    clb.set_label(unit, labelpad=-35, y=1.07, rotation=0)


def plot_sunshine_anomalie(df1, df2):
    df1 = get_daily_sunshine(df1)
    df2 = get_daily_sunshine(df2)

    diff = df1 - df2  # if diff > 0 means more sunshine on Schoeckl than in Graz
    plt.figure()
    plt.rc('font', size=20)
    width = 0.25

    #plt.rc('font', size=20)

    """"
    #days = np.arange(1, 31, 1)
    #df1.day
    #df1.SUX
    bar1 = ax.bar(df1.day, df1.SUX, width, color='r')

    bar2 = ax.bar(df2.day + width, df2.SUX, width, color='g')

    bar3 = ax.bar(diff.day + width * 2, diff.SUX, width, color='b')
    """
    plt.bar(diff.day, diff.SUX, label='diff')
    plt.ylabel('sunshine duration (h)')
    plt.xlabel('days in November 2006')
    plt.legend()
    #plt.legend((bar1, bar2, bar3), ('Schoeckl', 'Graz', 'diff'))

    return None

def plot_timeseries(ds, title, start_date="", end_date="", output_dir="", trend=""):
    """Plots time series of temperature data.

    Args:

        ds (xarray dataset): Data to be plotted. Must contain at
            least the parameter "t".
        title: Contains the title of the figure

        start_date (datetime.date): Input start date of the plotted ds

        end_date (datetime.date): Input end date of the plotted ds

        output_dir (string, default: ""): If string is not empty, save
            figure to this directory. If empty, just show the figure.

        trend: If given a linear trend is added to the plot
    """

    print("plot data")
    nr_years = end_date.year - start_date.year

    fig, ax = plt.subplots()
    ax.plot(ds.time, ds.t)
    if nr_years <= 2:
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    else:
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y"))
    plt.gca().xaxis.set_major_locator(mdates.AutoDateLocator())

    if trend:
        #Error analysis still missing :(
        ax.plot(ds.time, ds["trend"], label="linear trend")

    ax.set(title=title)
    plt.legend()
    ax.set_ylabel("t (K)")
    plt.show()

    if output_dir:
        timestamp = pd.Timestamp.now().strftime("%Y-%m-%dT%H:%M:%S")
        fn = Path(output_dir).joinpath(f"{timestamp}-timeseries.png")
        print(f"Save plot to: {fn}")
        plt.savefig(fn, dpi=200, bbox_inches="tight")
    else:
        plt.show()


#@njit()
def plot_scatter(ax, temp, height):
    plt.scatter(temp, height, s=1)

def plot_height_distribution(ds):
    """plots a bar plot of heights of the study region and orography
    input:
    ds (xarray): dataset containing data variable: dem
    ax: axis to plot
    output:
    plot:  distribution of heights"""

    height = ds.dem.values.ravel()
    bins = np.linspace(0, 4000, 9)
    counts, bins = np.histogram(height, bins=bins, range=(0, 4000))

    bins = bins[1:]
    bins = [z-250 for z in bins]
    plt.figure()
    plt.barh(bins, counts, height=300)
    plt.xlabel('number of grid points')
    plt.ylabel('h (m MSL)')
    plt.ylim([0, 4000])

def plot_inversion_comparisson(ds, ds_cclm):



    fig, axs = plt.subplots(nrows=2, ncols=2)  # TODO: make subplots automattically
    axs = axs.flatten()

    index = 0
    # ds.time.dt.day.values
    # [1, 28]
    for i, ax in zip([1, 7, 1, 7], axs):

        if index < 2:
            ds = ds
            Kelvin_to_Celcius = -273.15
        if index > 1:
            ds = ds_cclm
            Kelvin_to_Celcius = 0 #TODO: does not work ..temp is still in Kelvin


        # get upper level grid points uls for lapse rate estimation (grid points above 2000 m)
        ds_uls = ds.where(ds.orog > 2200, drop=True)  # does only reduce orog not the whole data set :(

        # make sure ground effects such as cold pools are neglected TODO: does not work this way
        ds = ds.where(ds.orog > 500, drop=True)

        ds_uls_ = ds_uls.sel(day=i)
        # time=ds.time.dt.day == i
        ds_ = ds.sel(day=i)

        temp_uls, height_uls = drop_nan_for_all_data_vars(ds_uls_.Tn + Kelvin_to_Celcius, ds_uls_.orog)

        gamma, T0 = calc_uls_lapse_rate(height_uls, temp_uls)

        #lin_ = np.linspace(np.min(height_uls), np.max(height_uls), 1000)
        #ax.plot(gamma * lin_ + T0, lin_, 'r')

        temp, height = drop_nan_for_all_data_vars(ds_.Tn + Kelvin_to_Celcius, ds_.orog)
        ax.scatter(temp, height, marker='.', s=5, alpha=0.2)
        lin_height = np.linspace(np.min(height), np.max(height), 1000)

        """
        p0 = [1, 1000, 1500]  # initial guess for [a, h0, h1]

        p, e = optimize.curve_fit(lambda y, a, h0, h1: piecewise_linear(y, a, h0, h1, gamma=gamma, T0=T0), height, temp, p0)

        lin_height = np.linspace(np.min(height), np.max(height), 1000) #TODO: set gamma and T0 fixed in fit does not lead to good results
        ax.plot(piecewise_linear(lin_height, *p, gamma, T0), lin_height, 'orange')
        """
        p0 = [1, 1000, 1500, gamma, T0]  # initial guess


        p, e = optimize.curve_fit(lambda y, a, h0, h1, gamma, T0: piecewise_linear(y, a, h0, h1, gamma, T0), height, temp, p0)
        h0 = p[1]
        h1 = p[2]
        a = p[0]
        ds = ds.assign(magnitude=ds.Tn / ds.Tn * a)
        ds['magnitude'].attrs["Units"] = "°C"
        ds = ds.assign(thickness=ds.Tn / ds.Tn * (h1 - h0))
        ds['thickness'].attrs["Units"] = "m"
        ds = ds.assign(intensity=ds.Tn / ds.Tn * (h1 - h0) / a)
        ds['intensity'].attrs["Units"] = "°C / m"

        # --------------------------------------------------------------------------------------------------------------
        # plot
        ax.plot(piecewise_linear(lin_height, *p), lin_height, 'g')
        ax.set_title(f'0{i}.{12}.{2009}')  # TODO: can be done better
        ax.set_xlabel("T (K)")
        ax.set_ylabel(f"h (m)")
        ax.label_outer()

        index += 1

    return None
