import numpy as np
import matplotlib.pyplot as plt
from input_output import read_data, get_all_datasets
from compute import background_temperature_field, daily_mean, low_level_temperature_field, \
    full_temperature_profile
from ds_transformations import slice_lon_lat, austrian_subregions, austrian_subregions_cclm, get_cclm_to_spartacus_area, \
    cordex_rotated_grid
from scipy import optimize
from plot import plot_2dmap, plot_height_distribution, plot_inversion_comparisson, plot_sunshine_anomalie
import xarray as xr
import cordex as cx

def main():
    """
    sunshine_file_schoeckl = "../data/Schoeckl_20061101T0000_20061130T2300.csv"
    ds_sunshine_schoeckl = read_data(sunshine_file_schoeckl)

    sunshine_file_graz = "../data/Graz_20061101T0000_20061130T2300.csv"
    ds_sunshine_graz = read_data(sunshine_file_graz)
    plot_sunshine_anomalie(ds_sunshine_schoeckl, ds_sunshine_graz)
    """

    #-------------------------------------------------------------------------------------------------------------------
    # filenames
    fn_cclm = "../data/lffd200912.nc_ET"  # coords lon,lat, time; data_vars: T_2M, orog

    #orographie_file = "../data/SPARTACUS_orographie_v21.nc"  # coords lon, lat data_vars: orog
    orographie_file = "../data/SPARTACUS-DAILY_DEM_MASK.nc"  # coords lon, lat, data_vars: dem

    ds_orog = read_data(orographie_file)

    fn = "../data/SPARTACUS_Tn200912.nc"  # coords lon, lat, time - not lon and lat as orographie; data_vars: Tn
    #fn = "../data/SPARTACUS-daily_20061110_20061130.nc"  # data_vars: Tn...daily min temp, Tx...daily max temp

    #-------------------------------------------------------------------------------------------------------------------
    #if only ds and not cclm is needed -> no riggridding
    ds = read_data(fn)
    ds = ds.assign(orog=ds_orog['dem'])  # data_vars: Tn, Tx and orog: height of lon lat
    ds = daily_mean(ds)
    #-------------------------------------------------------------------------------------------------------------------
    #ds, ds_cclm, cclm_daily_min = get_all_datasets(orographie_file, fn, fn_cclm)

    #-------------------------------------------------------------------------------------------------------------------
    # select only inversion days:
    inv_days_122009 = [4, 6, 7, 10, 21, 23]
    #inv_days_122009 = [6]
    ds = ds.sel(day=inv_days_122009)
    #cclm_daily_min = ds.sel(day=inv_days_122009)
    #-------------------------------------------------------------------------------------------------------------------

    #plot_height_distribution(ds_orog)
    #low_level_temperature_field_graz(cclm_daily_min)
    #south_cclm, nw_cclm, ne_cclm = austrian_subregions(cclm_daily_min)
    south, nw, ne = austrian_subregions(ds)

    #plot_inversion_comparisson(south, south_cclm)
    #low_level_temperature_field_graz(cclm_daily_min)
    #clm_daily_min = background_temperature_field(south_cclm)

    #-------------------------------------------------------------------------------------------------------------------
    # select the area of Graz
    ref = [47.186668, 15.421371]  # Graz umgebung
    # ref = [48.1218, 16.5633]  # Wien Schwechat Umgebung
    # ref = [48.2454052, 16.3560018]  # Wien Hohe Warte

    # 47.1935279, 15.4815976
    radius_lon = 0.12968063 * 3
    radius_lat = 0.00434875 * 3 # TODO: select exactly 10x10km -> 100 grid points including Schöckl
    ds = ds.where((ds.coords['lat'] > ref[0] - radius_lat) & (ds.coords['lat'] < ref[0] + radius_lat) &
                  (ds.coords['lon'] > ref[1] - radius_lon)
                  & (ds.coords['lon'] < ref[1] + radius_lon), drop=True)
    #-------------------------------------------------------------------------------------------------------------------
    #ds_btf = background_temperature_field(south)  #TODO: dont delete both work fine
    #low_level_temperature_field(ds)
    full_temperature_profile(ds, south)



    #ds1 = ds.sel(day=5)
    #ds2 = ds.sel(day=8)
    #plot_comparisson(ds1.Tn, ds2.Tn)

    plt.show()

if __name__ == "__main__":
    main()
