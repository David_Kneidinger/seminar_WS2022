import argparse
from pathlib import Path
from . import input_output, plot, compute
import datetime
import pathlib


def timeseries():
    """Entry point"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data-file",
        dest="data_file",
        required=True,
        help="Filename of input data.",
    )
    parser.add_argument(
        "--anomalies",
        dest="anomalies",
        action="store_true",
        help="Compute and plot anomalies, instead of absolute values.",
    )
    parser.add_argument(
        "--trend",
        dest="trend",
        action="store_true",
        help="Compute and plot linear trend",
    )
    parser.add_argument(
        "--start_date",
        dest="start_date",
        required=False,
        help="Select a timeseries start date in ISO-Format YYYY-MM-DD, default is 1991-01-01",
        type=datetime.date.fromisoformat,
        default="1991-01-01",
    )
    parser.add_argument(
        "--end_date",
        dest="end_date",
        required=False,
        help="Select a timeseries end date in ISO-Format YYYY-MM-DD, default is 2021-12-31",
        type=datetime.date.fromisoformat,
        default="2021-12-31",
    )
    parser.add_argument(
        "--latitude_range",
        dest="latitude_range",
        required=False,
        help="Select a latitude range, default is (-90, 90)",
        type=float,
        default=(-90, 90),
        nargs=2,
        metavar=("lat_min", "lat_max"),
    )
    parser.add_argument(
        "--output-dir",
        dest="output_dir",
        required=False,
        help="Directory to save plot files and netCDF output data to. If not given, show the figure but do not save it.",
    )

    args = parser.parse_args()

    if not Path(args.data_file).exists():
        parser.error(f"File {args.data_file} does not exist.")

    file_type = pathlib.Path(args.data_file).suffix
    if file_type != ".csv" and file_type != ".nc":
        print(pathlib.Path(args.data_file).suffix)
        parser.error(f"Please input a .csv or a .nc file")

    if args.output_dir and not Path(args.output_dir).exists():
        parser.error(f"Output directory {Path(args.output_dir)} does not exist.")
        # alternitive two different functions: read_csv and read_nc and call them her according to the extension

    data = io.read_data(
        args.data_file, args.start_date, args.end_date, args.latitude_range
    )

    if args.anomalies:
        data = compute.anomalies(data, args.start_date, args.end_date)
        if args.start_date == parser.get_default(
            "start_date"
        ) and args.end_date == parser.get_default("end_date"):
            title = f"Temperature anomalies for input timeseries"
        else:
            title = f"Temperature anomalies \n {args.start_date} - {args.end_date}"

    else:
        if args.start_date == parser.get_default(
            "start_date"
        ) and args.end_date == parser.get_default("end_date"):
            title = f"Timeseries of Temperature data for input timeseries"
        else:
            title = (
                f"Timeseries of Temperature data \n {args.start_date} - {args.end_date}"
            )

    if args.trend:
        data = compute.linear_trend(data, args.start_date, args.end_date)
        title = title + " with linear trend"

    plot.plot_timeseries(
        data, title, args.start_date, args.end_date, args.output_dir, args.trend
    )

    if args.output_dir:
        io.write_data(data, args.output_dir)
