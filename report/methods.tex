\chapter{Data and methods}
\label{ch:methods}
The below presented method to get vertical temperature profiles out of gridded datasets, containing 2 m temperatures, uses the fact that the area of study (Austria) covers a wide vertical range (\autoref{fig:orography}). Therefore the interpolation method presented in \textcite{Frei2014} can be used in a slightly modified version, since not measurement station-, but already the interpolated gridded data is used.

\begin{figure}[hbt!]
	\begin{subfigure}[h]{0.5\linewidth}
		\includegraphics[width=\linewidth]{figs/orog_ds.png}
		\caption{}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.5\linewidth}
		\includegraphics[width=\linewidth]{figs/height_distribution.png}
		\caption{}
	\end{subfigure}%
	\caption[Orography of Austria]{Left: Orography of the \ac{spartacus}. Right: Distribution of different heights of the grid points in the \ac{spartacus}.}
	\label{fig:orography}
\end{figure}

Although Austria covers a wide vertical height spectrum in \autoref{fig:orography} the amount of grid points below 1000 m m.s.l. dominates over the the higher grid points.

\subsection{Time of study}
High pressure system are essential for subsidence inversions to occur. In (especially in the winter months) periods of high pressure, fog formation in valleys and basins is more common, whereas outside hardly an overcast sky is present. For that reason the chosen month, should have many days of different sunshine durations for two measurement stations, where one is located in a basin and the other station is located on a mountain close by. This two stations are chosen to be: Graz University (367 m m.s.l.) and Graz Schöckl (1445 m m.s.l.). For that reason the December 2009 is chosen.

\begin{figure}[hbt!]
	\centering
	\includegraphics[width=\linewidth]{figs/sunshine_12_2009.png}	
	\caption[Sunshine duration difference Graz University and Schöckl]{12.2009: Sunshine time difference $t_{Graz} - t_{Schöckl}$ for the stations Graz University and Schöckl.}
	\label{fig:sunshine}
\end{figure}

It should be noted that, a month with many days of sunshine difference between these two stations are likely to have a few days with subsidence inversions, but inversion days not always lead to sunshine difference. 

\subsection{SPARTACUS Dataset}
For a reference data, the gridded observational climate dataset presented by \parencite{Hiebl2016}, \ac{spartacus}, is used. The Dataset contains daily minimum and maximum temperatures with a spatial resolution of 1x1 km. The area covers whole of Austria as well as some border areas.
Since the main purpose is to look at subsidence inversions the area covered by the dataset is big enough, additionally the small grid size may also be small enough to cover local effects such as radiation inversion in some cases. A big benefit of the \ac{spartacus} is, that effects due to inversion are already accounted in the interpolation method presented by \textcite{Hiebl2018}. 
Since inversions are detectable by only looking at daily minimum temperature, this is mainly because the strongest inversion occur in the morning and yield for the smallest temperatures of the day, mainly these are investigated in the further studies. The over- or underestimation effect of the inversion parameters, because of the different daytime's heating sets in for different altitudes, are estimated to be smaller than 1 K and therefore neglected in this investigation.

\subsection{CCLM}
\acf{cclm} is a non hydrostatic \ac{rcm}, which is based on the COSMO Model of the german weather service.  It uses regular latitude/longitude grid with a rotated pole. The simulation is driven by \ac{era5} data. The area covered by the data includes whole of Austria. The spatial resolution is 3x3 km. As orography the \ac{spartacus} orography in \autoref{fig:orography} is used in a regridded form.


\section{Method}
In order to make statements on inversions with the used gridded dataset, a vertical temperature profile is calculated. The first step is to create a nonlinear background temperature field ($T$ is a function of the height $z$). The used method follows closely \parencite{Frei2014}.
First Austria is divided into three subregions (\autoref{fig:orography}): south, north-west and north-east. It is shown in \textcite{Hiebl2016} that the division along the main alpine crest is a good choice to separate Austria into a north and a south domain. North-west and north-east are separated along the longitude $12.95$. This assures that in every domain a wide vertical spectrum is covered by grid points.

The next steps are performed for each subregion individually:
The background field is described by a non linear piecewise function:

\begin{equation}
	\label{eq:backgroundfield_fit}
	T(z) = \left\{
	\begin{array}{ll}
		T_0 - \gamma z & \quad z \geq h_1 \\
		T_0 - \gamma z - \frac{a}{2}\left(1 + \cos \left(\pi \cdot \frac{z -h_0}{h_1 - h_0}\right) \right) & \quad h_0 < z < h_1 \\
		T_0 - \gamma z - a
	\end{array}
	\right\}
\end{equation}

where $h_0$ describes the bottom and $h_1$ the top of the inversion layer.

This profile involves two linear section, both with a lapse rate $\gamma$, one at higher and one at lower levels, where $T_0$ denotes the intercept at altitude $z=0$ m of the upper level linear section. The two sections are shifted against each other by the parameter $a$. The middle part ($h_0 < z < h_1$) is fitted by a continues step function. This piecewise function allows a lot of different vertical temperatures profiles.

\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.6\textwidth]{figs/fit_function_sketch.jpeg}
	\caption[Sketch of the \ac{nbtfff}]{Left: schematic of the fit function \autoref{eq:backgroundfield_fit} used for the background temperature field. Right: Definition of the three inversion parameters:\textit{inversion thickness}, \textit{inversion intensity}, \textit{inversion magnitude}}
	\label{fig:fit_function}
\end{figure}
Since local effects may occur near surface, such as cold-pool formation in valleys, which could effect the fit in a major way, a separate fit is performed using only upper level grid points (grid points above $2000$ m m.s.l.). Additionally the grid points below 500 m m.s.l. are not taken into account. With these upper level grid points, the lapse rate $\gamma$ and the interception temperature $T_0$ are computed. This two parameters can be used as an initial guess for a fit of the \ac{nbtfff}. With the fit parameters $h_0$, $h_1$ and $a$, three inversion parameters can be defined. The \textit{inversion thickness} is given be the difference in the top and the bottom of the inversion layer ($h_1 - h_0$). The \textit{inversion intensity} is given be the linearized slope of the continuous step function ($\frac{h_1 - h_0}{a}$). And the \textit{inversion magnitude} given by the parameter $a$, describing the difference between the maximum and the minimum inversion within the inversion layer.


The lower level temperature profiles are computed separately for smaller subdomains of 10x10 km. The vertical extend of this subdomain is expanded upward by filling the missing values with the ones of a wider spatial area of 30x30 km. In order to get a complete profile up to 3000 m m.s.l., the temperature background field is used. 
Since this procedure is computationally very expansive, its only done for the area around Graz. 
In \textcite{Hiebl2018} in every subdomain 100 grid points are allocated to altitudinal zone of 100 m thickness and averaged over the given altitudinal zone. This is done because otherwise the high frequency of grid points below 1000 m m.s.l. (\autoref{fig:orography}) leads to an overestimation of the lower points in the fit. 


In the master thesis \textcite{Foedermann2017} it is shown, that neither \ac{spartacus} nor \ac{cclm} simulations are capable of capturing small scale radiation inversion. But the WegenerNet, a very dens gridded observation network in the Feldbach region \footnote{\url{https://wegenernet.org/portal/v7.1/2022/1}}, can be used for studies of radiation inversions, but are not explicitly investigated in this work.
